package rs.vlasi.gatemanagement.web.api.admin

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.gatemanagement.domain.gate.GateAdministrationService
import rs.vlasi.gatemanagement.web.dto.RestResponse
import rs.vlasi.gatemanagement.web.dto.request.GateAvailabilityRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse

@RestController
@RequestMapping("api/admin/v1/gates")
class AdminGateController(
    private val gateAdministrationService: GateAdministrationService
) {

    @PutMapping("/{gateCode}/availability")
    fun updateGateAvailability(@PathVariable gateCode: String, @RequestBody request: GateAvailabilityRequest): RestResponse<GateResponse> {
        return RestResponse(gateAdministrationService.updateGateAvailability(gateCode, request))
    }

}
