package rs.vlasi.gatemanagement.domain.gate.impl

import org.springframework.stereotype.Service
import rs.vlasi.gatemanagement.data.mapper.GateMapper
import rs.vlasi.gatemanagement.data.repository.FlightRepository
import rs.vlasi.gatemanagement.data.repository.GateRepository
import rs.vlasi.gatemanagement.domain.gate.GateAssigmentService
import rs.vlasi.gatemanagement.exceptions.NoAvailableGates
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.utils.DateTimeService
import rs.vlasi.gatemanagement.web.dto.request.OccupyGateRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse
import javax.transaction.Transactional

@Service
class GateAssigmentServiceImpl(
    val gateRepository: GateRepository,
    val flightRepository: FlightRepository,
    val dateTimeService: DateTimeService
) : GateAssigmentService {

    @Transactional
    override fun occupyAnyGate(request: OccupyGateRequest): GateResponse {
        val currentTime = dateTimeService.nowLocalTime()
        val gateModel = gateRepository
            .findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(
                currentTime,
                currentTime
            ) ?: throw NoAvailableGates("No Available gates")

        val flightModel = flightRepository
            .findByCode(
                request.flightCode
            ) ?: throw NotFound("No Flight with ${request.flightCode} Found")

        gateModel.flight = flightModel
        flightModel.gate = gateModel

        return GateMapper.toResponseDto(gateModel)
    }

    @Transactional
    override fun releaseGate(gateCode: String): GateResponse {
        val gateModel = gateRepository
            .findByCode(
                gateCode
            ) ?: throw NotFound("No gate with code $gateCode exists")

        gateModel.flight = null

        return GateMapper.toResponseDto(gateModel)
    }
}
