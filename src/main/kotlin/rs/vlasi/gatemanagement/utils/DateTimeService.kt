package rs.vlasi.gatemanagement.utils

import org.springframework.stereotype.Service
import java.time.LocalTime

@Service
class DateTimeService {

    fun nowLocalTime() = LocalTime.now()

}
