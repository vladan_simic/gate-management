package rs.vlasi.gatemanagement.data.mapper


import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import rs.vlasi.gatemanagement.data.model.FlightModel
import rs.vlasi.gatemanagement.data.model.GateModel
import java.time.LocalTime

internal class GateMapperTest {

    @Nested
    inner class ToGateResponse {

        @Test
        fun `should return gate response`() {

            val result = GateMapper.toResponseDto(
                gateModel = GateModel(
                    code = "GT",
                    flight = FlightModel(code = "FLG"),
                    availableFrom = LocalTime.of(8,0),
                    availableTo = LocalTime.of(23,0)
                )
            )
            assertEquals(
                "GT",
                result.gateCode
            )

            assertEquals(
                "FLG",
                result.assignedFlight
            )

            assertEquals(
                LocalTime.of(8,0),
                result.availableFrom
            )

            assertEquals(
                LocalTime.of(23,0),
                result.availableTo
            )
        }

    }

}
