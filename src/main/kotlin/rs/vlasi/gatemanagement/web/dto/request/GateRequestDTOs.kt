package rs.vlasi.gatemanagement.web.dto.request

import java.time.LocalTime


data class OccupyGateRequest(
    val flightCode: String
)

data class GateAvailabilityRequest(
    val from: LocalTime,
    val to: LocalTime
)
