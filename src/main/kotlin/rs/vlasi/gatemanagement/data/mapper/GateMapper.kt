package rs.vlasi.gatemanagement.data.mapper

import rs.vlasi.gatemanagement.data.model.GateModel
import rs.vlasi.gatemanagement.exceptions.InternalServerError
import rs.vlasi.gatemanagement.web.dto.response.GateResponse

object GateMapper {

    fun toResponseDto(gateModel: GateModel) =
        GateResponse(
            assignedFlight = gateModel.flight?.code,
            gateCode = gateModel.code ?: throw InternalServerError("No gate code exists"),
            availableFrom = gateModel.availableFrom,
            availableTo = gateModel.availableTo
        )
}
