package rs.vlasi.gatemanagement.exceptions

open class NoAvailableGates(message: String?) : NotFound(message) {
}
