package rs.vlasi.gatemanagement.config

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import rs.vlasi.gatemanagement.data.model.FlightModel
import rs.vlasi.gatemanagement.data.model.GateModel
import rs.vlasi.gatemanagement.data.repository.FlightRepository
import rs.vlasi.gatemanagement.data.repository.GateRepository
import java.time.LocalDateTime
import java.time.LocalTime

@Configuration
class DBInit {

    @Bean
    fun databaseInitializer(
        gateRepository: GateRepository,
        flightRepository: FlightRepository
    ) = ApplicationRunner {

       gateRepository.save(GateModel(code = "GT00"))
       gateRepository.save(
            GateModel(
                code = "GT01",
                availableTo = LocalTime.of(23,0),
                availableFrom = LocalTime.of(8,0)
            ),
        )

        flightRepository.save(FlightModel(code = "FLG00"))
        flightRepository.save(FlightModel(code = "FLG01"))
        flightRepository.save(FlightModel(code = "FLG02"))
        flightRepository.save(FlightModel(code = "FLG03"))

    }

}
