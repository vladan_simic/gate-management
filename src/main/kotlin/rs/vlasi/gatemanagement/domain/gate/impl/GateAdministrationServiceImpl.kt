package rs.vlasi.gatemanagement.domain.gate.impl

import org.springframework.stereotype.Service
import rs.vlasi.gatemanagement.data.mapper.GateMapper
import rs.vlasi.gatemanagement.data.repository.GateRepository
import rs.vlasi.gatemanagement.domain.gate.GateAdministrationService
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.web.dto.request.GateAvailabilityRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse
import javax.transaction.Transactional

@Service
class GateAdministrationServiceImpl(
    private val gateRepository: GateRepository
) : GateAdministrationService {

    @Transactional
    override fun updateGateAvailability(gateCode: String, request: GateAvailabilityRequest): GateResponse {
        val gateToUpdate = gateRepository
            .findByCode(
                gateCode
            ) ?: throw NotFound("No gate with code $gateCode found")


        gateToUpdate.availableFrom = request.from
        gateToUpdate.availableTo = request.to

        return GateMapper.toResponseDto(gateToUpdate)
    }
}
