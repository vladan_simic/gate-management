package rs.vlasi.gatemanagement.domain.gate.impl


import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import rs.vlasi.gatemanagement.data.model.GateModel
import rs.vlasi.gatemanagement.data.repository.GateRepository
import rs.vlasi.gatemanagement.domain.gate.GateAdministrationService
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.web.dto.request.GateAvailabilityRequest
import java.time.LocalTime

internal class GateAdministrationServiceImplTest {

    private val gateRepository: GateRepository = mockk()

    private val gateAdministrationService: GateAdministrationService = GateAdministrationServiceImpl(
        gateRepository = gateRepository
    )

    @AfterEach
    fun reset() {
        clearAllMocks()
    }

    @Nested
    inner class UpdateGateAvailability {

        @Test
        fun `should throw not found when not gate with code exists`() {
            every {
                gateRepository.findByCode("NO-SUCH")
            }.returns(null)

            assertThrows<NotFound> {
                gateAdministrationService
                    .updateGateAvailability(
                        gateCode = "NO-SUCH",
                        request = GateAvailabilityRequest(
                            from = LocalTime.of(8, 0),
                            to = LocalTime.of(18, 0)
                        )
                    )
            }
        }

        @Test
        fun `should update gate with new availability`() {
            every {
                gateRepository.findByCode("GT00")
            }.returns(GateModel(
                code = "GT00"
            ))

            val result = gateAdministrationService
                .updateGateAvailability(
                    gateCode = "GT00",
                    request = GateAvailabilityRequest(
                        from = LocalTime.of(8, 0),
                        to = LocalTime.of(18, 0)
                    )
                )

            Assertions.assertEquals(
                "GT00",
                result.gateCode
            )

            Assertions.assertEquals(
                LocalTime.of(8, 0),
                result.availableFrom
            )

            Assertions.assertEquals(
                LocalTime.of(18, 0),
                result.availableTo
            )

        }

    }

}
