package rs.vlasi.gatemanagement.domain.gate

import rs.vlasi.gatemanagement.web.dto.request.GateAvailabilityRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse

interface GateAdministrationService {

    /**
     * Update gate availability time
     */
    fun updateGateAvailability(gateCode: String, request: GateAvailabilityRequest): GateResponse


}
