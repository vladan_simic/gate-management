# Read Me First

- Project represents Gate management system designed as part of task specified under docs directory.
It has 3 main functionalities
  - occupy a first available gate with flight
  - release given gate
  - update gate availability

Assumption:
  No CRUD operations on Gate/ Flight resources are implemented as it was not required by the task.
Initial data is added via DBInit 

# Running instructions

    Prerequisite:
        - Java 8

    - Build and Start Application:
    `gradlew build`
    java -jar /build/libs/gate-management-0.0.1-SNAPSHOT.jar

### Technologies/Tools 

- [Spring Boot]
- [Spring Data]
- [Kotlin]
- [Postman]

### Additional improvements

- Add Spring Security and make Admin endpoints secured
- Add Open API specification
- Dockerize application
- Introduce different envs for Postman collection
- Add integration tests
- Add validation for API requests ( check for from and to time to be correct)
