package rs.vlasi.gatemanagement.data.model

import java.time.LocalTime
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table(name = "FLIGHT")
class FlightModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @Column(name = "code", unique = true)
    var code: String? = null,
    @OneToOne
    var gate: GateModel? = null
)

@Entity
@Table(name = "GATE")
class GateModel(
    @Id @GeneratedValue
    var id: Long? = null,
    @Column(name = "code", unique = true)
    var code: String? = null,

    @Column(name = "available_from")
    var availableFrom: LocalTime? = null,

    @Column(name = "available_to")
    var availableTo: LocalTime? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "flight_id")
    var flight: FlightModel? = null
)
