package rs.vlasi.gatemanagement.data.model.repositories

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import rs.vlasi.gatemanagement.data.model.FlightModel
import rs.vlasi.gatemanagement.data.model.GateModel
import rs.vlasi.gatemanagement.data.repository.FlightRepository
import rs.vlasi.gatemanagement.data.repository.GateRepository
import java.time.LocalTime

@DataJpaTest
class RepositoriesTest @Autowired constructor(
    val entityManager: TestEntityManager,
    val gateRepository: GateRepository,
    val flightRepository: FlightRepository
) {

    @Nested
    inner class GateRepo {

        @Test
        fun `should find first available in time slot`() {

            entityManager.persist(
                GateModel(
                    code = "CDX001",
                    availableTo = LocalTime.of(18, 0),
                    availableFrom = LocalTime.of(8, 0)
                )
            )

             entityManager.persist(
                GateModel(
                    code = "CDX002",
                    availableTo = LocalTime.of(12, 0),
                    availableFrom = LocalTime.of(8, 0)
                )
            )

            val gateFound =
                gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(
                    LocalTime.of(12, 0),
                    LocalTime.of(12, 0)
                )


            Assertions.assertEquals(
                "CDX001",
                gateFound?.code
            )


        }

        @Test
        fun `should find first available in time slot event if availability is null`() {

            entityManager.persist(
                GateModel(
                    code = "CDX001"
                )
            )



            val gateFound =
                gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(
                    LocalTime.of(12, 0),
                    LocalTime.of(12, 0)
                )


            Assertions.assertEquals(
                "CDX001",
                gateFound?.code
            )


        }


        @Test
        fun `should get available gate`() {
            val firstGate = GateModel(
                code = "CDX001",
                availableTo = LocalTime.of(18, 0),
                availableFrom = LocalTime.of(8, 0)
            )

            val secondGate = GateModel(
                code = "CDXX002"
            )

            entityManager.persist(firstGate)
            entityManager.persist(secondGate)
            entityManager.flush()

            val found = gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(
                LocalTime.of(17, 25),
                LocalTime.of(17, 25)
            )

            Assertions.assertEquals(
                "CDX001",
                found?.code
            )

            Assertions.assertEquals(
                LocalTime.of(8, 0),
                found?.availableFrom
            )

            Assertions.assertEquals(
                LocalTime.of(18, 0),
                found?.availableTo
            )
        }


        @Test
        fun `should update gate availability`() {
            val firstGate = GateModel(
                code = "CDXX",
                availableTo = LocalTime.of(23, 0),
                availableFrom = LocalTime.of(8, 0)
            )

            entityManager.persist(firstGate)
            entityManager.flush()

            val found = gateRepository.findByCode("CDXX")

            Assertions.assertEquals(
                LocalTime.of(8, 0),
                found?.availableFrom
            )

            Assertions.assertEquals(
                LocalTime.of(23, 0),
                found?.availableTo
            )
        }

        @Test
        fun `should save and find gate by code`() {
            val firstGate = GateModel(code = "CDX1")
            entityManager.persist(firstGate)
            entityManager.flush()
            val found = gateRepository.findByCode("CDX1")
            assertThat(found).isEqualTo(firstGate)
        }

        @Test
        fun `should have not find gate with given code`() {
            val found = gateRepository.findByCode("CDX")
            assertThat(found).isEqualTo(null)
        }

        @Test
        fun `should save flight to given gate`() {
            val firstGate = GateModel(code = "CDX0")
            entityManager.persist(firstGate)
            val firstPlane = FlightModel(code = "FLG0", gate = firstGate)
            entityManager.persist(firstPlane)

            firstGate.flight = firstPlane
            entityManager.persist(firstGate)

            entityManager.flush()
            val found = flightRepository.findByCode("FLG0")
            assertThat(found?.gate).isEqualTo(firstGate)

            val foundGate = gateRepository.findByCode("CDX0")
            assertThat(foundGate?.flight).isEqualTo(firstPlane)
        }

    }


    @Nested
    inner class FlightRepo {
        @Test
        fun `should save and find flight by code`() {
            val firstPlane = FlightModel(code = "FLG1")
            entityManager.persist(firstPlane)
            entityManager.flush()
            val found = flightRepository.findByCode("FLG1")
            assertThat(found).isEqualTo(firstPlane)
        }

        @Test
        fun `should have not find flight with given code`() {
            val found = flightRepository.findByCode("FLG666")
            assertThat(found).isEqualTo(null)
        }

    }


}
