package rs.vlasi.gatemanagement.domain.gate.impl


import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import rs.vlasi.gatemanagement.data.model.FlightModel
import rs.vlasi.gatemanagement.data.model.GateModel
import rs.vlasi.gatemanagement.data.repository.FlightRepository
import rs.vlasi.gatemanagement.data.repository.GateRepository
import rs.vlasi.gatemanagement.domain.gate.GateAssigmentService
import rs.vlasi.gatemanagement.exceptions.NoAvailableGates
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.utils.DateTimeService
import rs.vlasi.gatemanagement.web.dto.request.OccupyGateRequest

internal class GateAssigmentServiceImplTest {

    private val gateRepository: GateRepository = mockk()
    private val flightRepository: FlightRepository = mockk()
    private val dateTimeService:DateTimeService = DateTimeService()

    private val gateAssigmentService: GateAssigmentService = GateAssigmentServiceImpl(
        flightRepository = flightRepository,
        gateRepository = gateRepository,
        dateTimeService = dateTimeService
    )

    @AfterEach
    fun reset() {
        clearAllMocks()
    }

    @Nested
    inner class ReleaseGate {

        @Test
        fun `should throw not found when not gate with code exists`() {
            every {
                gateRepository.findByCode("NO-SUCH")
            }.returns(null)


            assertThrows<NotFound> {
                gateAssigmentService
                    .releaseGate(
                        gateCode = "NO-SUCH"
                    )
            }
        }


        @Test
        fun `should return gate response`() {
            every {
                gateRepository.findByCode("GT10")
            }.returns(GateModel(code = "GT10", flight = FlightModel(code = "FLG10")))

            val response = gateAssigmentService
                .releaseGate(
                    gateCode = "GT10"
                )


            assertEquals(
                "GT10",
                response.gateCode
            )

            assertNull(
                response.assignedFlight
            )
        }
    }


    @Nested
    inner class OccupyAnyGate {

        @Test
        fun `should should throw no gate available exception when no gates available`() {
            every {
                gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(any(),any())
            }.returns(null)


            assertThrows<NoAvailableGates> {
                gateAssigmentService
                    .occupyAnyGate(
                        request = OccupyGateRequest(
                            flightCode = "FLG00"
                        )
                    )
            }
        }


        @Test
        fun `should should throw not found exception when no flight exists by code`() {
            every {
                gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(any(),any())
            }.returns(GateModel())

            every {
                flightRepository.findByCode(code = "FLG01")
            }.returns(null)


            assertThrows<NotFound> {
                gateAssigmentService
                    .occupyAnyGate(
                        request = OccupyGateRequest(
                            flightCode = "FLG01"
                        )
                    )

            }
        }

        @Test
        fun `should return gate response`() {
            every {
                gateRepository.findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(any(),any())
            }.returns(GateModel(code = "GT00"))

            every {
                flightRepository.findByCode(code = "FLG02")
            }.returns(FlightModel(code = "FLG02"))


            val result = gateAssigmentService
                .occupyAnyGate(
                    request = OccupyGateRequest(
                        flightCode = "FLG02"
                    )
                )
            assertEquals("GT00", result.gateCode)
            assertEquals("FLG02", result.assignedFlight)
        }
    }
}


