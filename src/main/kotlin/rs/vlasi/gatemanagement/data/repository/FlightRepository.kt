package rs.vlasi.gatemanagement.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.gatemanagement.data.model.FlightModel

interface FlightRepository : JpaRepository<FlightModel, Long> {

    fun findByCode(code:String):FlightModel?
}
