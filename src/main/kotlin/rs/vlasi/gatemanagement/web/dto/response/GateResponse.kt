package rs.vlasi.gatemanagement.web.dto.response

import java.time.LocalTime

data class GateResponse(
    val gateCode: String,
    val assignedFlight: String? = null,
    val availableFrom: LocalTime? = null,
    val availableTo: LocalTime? = null
)
