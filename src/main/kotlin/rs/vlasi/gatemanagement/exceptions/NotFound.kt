package rs.vlasi.gatemanagement.exceptions

import java.lang.RuntimeException

open class NotFound(message: String?) : RuntimeException(message) {
}
