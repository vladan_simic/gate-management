package rs.vlasi.gatemanagement.web.handlers

import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import rs.vlasi.gatemanagement.exceptions.InternalServerError
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.web.dto.RestResponse
import java.util.UUID

@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

    private val log = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)


    @ExceptionHandler(value = [NotFound::class])
    @ResponseBody
    fun notFound(exception: NotFound, request: WebRequest): ResponseEntity<Any> {
        return handleException(exception, HttpStatus.NOT_FOUND, request) { message ->
            log.warn(message, exception)
        }
    }


    @ExceptionHandler(value = [InternalServerError::class])
    @ResponseBody
    fun internalServerError(exception: InternalServerError, request: WebRequest): ResponseEntity<Any> {
        return handleException(exception, HttpStatus.INTERNAL_SERVER_ERROR, request) { message ->
            log.error("[FATAL]  ::  $message", exception)
        }
    }



    @ExceptionHandler(value = [Throwable::class])
    @ResponseBody
    fun unspecifiedThrowable(exception: Exception, request: WebRequest): ResponseEntity<Any> {
        return handleException(exception, HttpStatus.INTERNAL_SERVER_ERROR, request) { message ->
            log.error("[UNSPECIFIED_THROWABLE]  ::  $message", exception)
        }
    }


    private fun handleException(
        exception: Exception,
        status: HttpStatus,
        request: WebRequest,
        logging: (String) -> Unit
    ): ResponseEntity<Any> {
        val errorCode = generateErrorId()
        generateLogMessage(errorCode, exception.message.orEmpty(), logging)
        return handleExceptionInternal(
            exception,
            createErrorBody(exception.message.orEmpty(), errorCode, status),
            HttpHeaders.EMPTY,
            status,
            request
        )
    }

    private fun generateErrorId(): String = "#" + UUID.randomUUID().toString().replace("-", "").takeLast(7)

    private fun generateLogMessage(errorCode: String, message: String, logging: (String) -> Unit) {
        logging("\n\nError-Code: $errorCode\n$message:")
    }

    private fun createErrorBody(message: String, errorCode: String, status: HttpStatus): RestResponse<ErrorDescription> = RestResponse(
        ErrorDescription(message, errorCode, status.value()),
        "status" to status.value(),
        "statusName" to status.name,
        "error" to true
    )

    data class ErrorDescription(
        val message: String,
        val errorCode: String,
        val status: Int
    )
}
