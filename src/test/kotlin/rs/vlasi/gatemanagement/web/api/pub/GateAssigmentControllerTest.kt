package rs.vlasi.gatemanagement.web.api.pub


import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import rs.vlasi.gatemanagement.domain.gate.GateAssigmentService
import rs.vlasi.gatemanagement.exceptions.NotFound
import rs.vlasi.gatemanagement.web.dto.request.OccupyGateRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse
import rs.vlasi.gatemanagement.web.handlers.GlobalExceptionHandler

internal class GateAssigmentControllerTest {

    private lateinit var mockMvc: MockMvc

    private val gateAssigmentService: GateAssigmentService = mockk()

    @AfterEach
    fun reset() {
        clearAllMocks()
    }

    @BeforeEach
    fun setup() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(GateAssigmentController(gateAssigmentService))
            .setControllerAdvice(GlobalExceptionHandler())
            .build()
    }

    @Nested
    inner class ReleaseGate {

        @Test
        fun `should return 404 status when no gate exists`() {
            every {
                gateAssigmentService.releaseGate("GT00")
            }.throws(NotFound("No resource"))

            mockMvc
                .perform(
                    put(
                        "/api/v1/gates/GT00/release"
                    )
                )
                .andExpect(
                    status().isNotFound
                )
        }

        @Test
        fun `should return 200 status gate occupied`() {
            every {
                gateAssigmentService.releaseGate("GT00")
            }.returns(
                GateResponse(
                    gateCode = "GT00",
                    assignedFlight = null
                )
            )

            mockMvc
                .perform(
                    put(
                        "/api/v1/gates/GT00/release"
                    )
                )
                .andExpect(
                    status().isOk
                )

        }
    }



    @Nested
    inner class OccupyAnyGate {

        @Test
        fun `should return 404 status when no gate exists`() {
            every {
                gateAssigmentService.occupyAnyGate(OccupyGateRequest("FLG00"))
            }.throws(NotFound("No resource"))

            mockMvc
                .perform(
                    post(
                        "/api/v1/gates/occupy"
                    ).content(
                        "{\"flightCode\": \"FLG00\"}"
                    ).contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                    status().isNotFound
                )
        }

        @Test
        fun `should return 200 status gate occupied`() {
            every {
                gateAssigmentService.occupyAnyGate(OccupyGateRequest("FLG00"))
            }.returns(
                GateResponse(
                    gateCode = "GT00",
                    assignedFlight = "FLG00"
                )
            )

            mockMvc
                .perform(
                    post(
                        "/api/v1/gates/occupy"
                    ).content(
                        "{\"flightCode\": \"FLG00\"}"
                    ).contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                    status().isOk
                )

        }
    }


}
