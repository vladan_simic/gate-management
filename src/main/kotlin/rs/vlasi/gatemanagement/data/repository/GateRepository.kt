package rs.vlasi.gatemanagement.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import rs.vlasi.gatemanagement.data.model.GateModel
import java.time.LocalTime

interface GateRepository : JpaRepository<GateModel, Long> {

    fun findFirstByFlightIsNullAndAvailableFromLessThanAndAvailableToGreaterThanOrFlightIsNullAndAvailableFromIsNullAndAvailableToIsNull(
        from: LocalTime,
        to: LocalTime
    ): GateModel?

    fun findByCode(code: String): GateModel?
}
