package rs.vlasi.gatemanagement.web.api.pub

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.vlasi.gatemanagement.domain.gate.GateAssigmentService
import rs.vlasi.gatemanagement.web.dto.RestResponse
import rs.vlasi.gatemanagement.web.dto.request.OccupyGateRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse


@RestController
@RequestMapping("api/v1/gates")
class GateAssigmentController(
    val gateAssigmentService: GateAssigmentService
) {

    @PostMapping("/occupy")
    fun occupyAnyGate(@RequestBody request: OccupyGateRequest): RestResponse<GateResponse> {
        return RestResponse(gateAssigmentService.occupyAnyGate(request))
    }

    @PutMapping("/{gateCode}/release")
    fun releaseGate(@PathVariable gateCode: String): RestResponse<GateResponse> {
        return RestResponse(gateAssigmentService.releaseGate(gateCode))
    }


}
