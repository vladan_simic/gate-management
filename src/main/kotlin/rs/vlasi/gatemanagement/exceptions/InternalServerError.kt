package rs.vlasi.gatemanagement.exceptions

import java.lang.RuntimeException

open class InternalServerError(message: String?) : RuntimeException(message) {
}
