package rs.vlasi.gatemanagement

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GateManagementApplication

fun main(args: Array<String>) {
	runApplication<GateManagementApplication>(*args)
}
