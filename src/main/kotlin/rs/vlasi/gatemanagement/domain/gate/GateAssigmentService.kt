package rs.vlasi.gatemanagement.domain.gate

import rs.vlasi.gatemanagement.web.dto.request.OccupyGateRequest
import rs.vlasi.gatemanagement.web.dto.response.GateResponse

interface GateAssigmentService {

    /**
     * Assign flight to any available gate.
     * In case of no available gates, exception is thrown
     */
    fun occupyAnyGate(request: OccupyGateRequest): GateResponse


    /**
     * Mark given gate as available
     */
    fun releaseGate(gateCode: String): GateResponse

}
